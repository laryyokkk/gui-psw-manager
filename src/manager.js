const crypto = require('./crypto');
const fileManager = require('./fileManager');

// ------------------------------------------------------------------------------------------------

const path = '/data.txt';
const byteStr = 'base64'

// ------------------------------------------------------------------------------------------------

function requireDataFromFile() {
    let fileContent = fileManager.GetDataFromFile(path);
    let decrypted = crypto.Decryption(fileContent, 'temp').toString(byteStr);
    let temp = convertStringToJSON(decrypted);
    return temp;
}

function saveDataToFile(tempJSON) {
    let data = convertJSONtoString(tempJSON);
    let encrypted = crypto.Encryption(data, 'temp');
    fileManager.SaveDataToFile(path, encrypted.toString(byteStr))
}

// ------------------------------------------------------------------------------------------------

function addStandardAccToJSON(tempJSON, company, login, password, address, mnemonic, link, restore_key, notes) {
    if (tempJSON != undefined && company != undefined) {
        tempJSON.collections.push({
            company: company,
            login: login,
            password: password,
            address: address,
            mnemonic: mnemonic,
            link: link,
            restore_key: restore_key,
            notes: notes
        });
        console.log('\nДобавленна новая коллекция\n');
    } else {
        console.log('\nНе добавленна новая коллекция\n');
    }
    return tempJSON;
}

function modifyByID(tempJSON, keyTo, data, id) {
    if (tempJSON != null && tempJSON.collections.length != 0) {
        for (key in tempJSON.collections[id]) {
            if (tempJSON.collections[id].hasOwnProperty(key)) {
                if (key == keyTo) {
                    tempJSON.collections[id][key] = data;
                    return tempJSON;
                }
            }
        }
    }
}
// ------------------------------------------------------------------------------------------------


function getAccByAddress(tempJSON, address) {
    let is = false;
    let temp = '';
    if (tempJSON != null && tempJSON.collections.length != 0) {
        for (var i = 0; i <= tempJSON.collections.length - 1; i++) {
            for (key in tempJSON.collections[i]) {
                if (tempJSON.collections[i].hasOwnProperty(key)) {
                    if (key == 'address' && tempJSON.collections[i][key].toLowerCase() == address.toLowerCase()) {
                        is = true;
                        temp += '\n-----------------'
                    }
                    if (is == true) {
                        temp += '\n' + key + ": " + tempJSON.collections[i][key];
                    }
                }
            }
            is = false;
        }
        return (temp != '') ? temp : '\nДанные отсутствуют\n';
    }
}

function findAcc(data, toFind){
    let temp = new Array();
    var regexp = new RegExp(`${toFind.toLowerCase()}.*`);
    if (data != null && data.collections.length != 0) {
        for (var i = 0; i <= data.collections.length; i++) {
            for (key in data.collections[i]) {
                if (data.collections[i].hasOwnProperty(key) && data.collections[i][key].toLowerCase() != ''){
                    if (regexp.test(data.collections[i][key].toLowerCase()) && toFind.length > 1) {
                        temp.push(i);
                    }
                }
            }
        }
        return temp ? temp : -1;
    }
}

function getAccByID(tempJSON, ID) {
    let temp = '';
    if (tempJSON != null && tempJSON.collections.length != 0) {
        for (key in tempJSON.collections[ID]) {
            if (tempJSON.collections[ID][key] != undefined) {
                temp += '\n' + key + ": " + tempJSON.collections[ID][key];
            }
        }
        return (temp != '') ? temp : '\nДанные отсутствуют\n';
    }
}

function deleteAccByID(data, id){
    if (data){
        delete data.collections[id];
    }
    console.log(data.collections);
    console.log('acc not del');
    return data
}
// ------------------------------------------------------------------------------------------------

// Можно добавить сюда hash авторизации
function initJSON(user = 'Me') {
    let tempJSON;

    try {
        tempJSON = requireDataFromFile();
    } catch (error) {
        tempJSON = {
            user: user,
            collections: []
        };
    }

    return tempJSON;
}

function convertJSONtoString(json) {
    return JSON.stringify(json);
}

function convertStringToJSON(string) {
    return JSON.parse(string);
}

// ------------------------------------------------------------------------------------------------

module.exports = {
    addStandardAccToJSON: addStandardAccToJSON,
    saveDataToFile: saveDataToFile,
    requireDataFromFile: requireDataFromFile,
    initJSON: initJSON,
    getAccByAddress: getAccByAddress,
    getAccByID: getAccByID,
    modifyByID: modifyByID,
    findAcc: findAcc,
    deleteAccByID: deleteAccByID
}