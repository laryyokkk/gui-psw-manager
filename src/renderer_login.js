const {
    ipcRenderer
} = require("electron")

const login = document.getElementById("login")

login.addEventListener("submit", async (event) => {
    event.preventDefault();

    ipcRenderer.send('checkAuthorization', {
        authorization: document.getElementsByName('input_password')[0].value
    });
});