const fs = require('fs');


// /Applications/password-manager.app/Contents/Resources/app.asar/fileManager.js
let dir = '/Applications/Password\ Manager.app/Contents/Resources';

try {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
        console.log("Directory is created.");
    }
} catch (err) {
    console.log(err);
}

function SaveDataToFile(filePath, data) {
    try {
        fs.writeFileSync(dir + filePath, data, 'utf8');
        console.log('saved: ' + filePath);
    } catch (err) {
        console.log(err);
    }
}

function GetDataFromFile(filePath) {
    try {
        let data = fs.readFileSync(dir + filePath, 'utf8');
        console.log('requiered: ' + filePath)
        return data;
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    SaveDataToFile: SaveDataToFile,
    GetDataFromFile: GetDataFromFile
}