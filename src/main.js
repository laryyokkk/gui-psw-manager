// How to build https://github.com/electron-userland/electron-builder

const {
  app,
  BrowserWindow,
  ipcMain,
} = require("electron")
const fs = require('fs');

var crypto = require('./crypto');

var mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 700,

    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      contextIsolation: false,
    }
  })
  // --------------------------------------------
  try {
    let key = fs.readFileSync('/Applications/Password\ Manager.app/Contents/Resources/public.key');
    // Если ключ пустой то не откроет страницу
    if (key) mainWindow.loadFile('./src/login.html');
  } catch (err) {
    mainWindow.loadFile('./src/register.html');
  }
  // --------------------------------------------

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// --------------------------------------------
ipcMain.on('index.html', () => {
  mainWindow.loadFile('./src/index.html');
})

ipcMain.on('newUser', (event, args) => {
  crypto.GenerateKeys();
  crypto.GenerateAuthorization(args.authorization);
  mainWindow.loadFile('./src/index.html');
});

ipcMain.on('checkAuthorization', (event, args) => {
  if (crypto.CheckAuthorization(args.authorization)) {
    mainWindow.loadFile('./src/index.html');
  }
});
// --------------------------------------------