const {
    ipcRenderer
} = require("electron");

const login = document.getElementById('login');

login.addEventListener("submit", async (event) => {
    event.preventDefault();
    var input_password_1 = document.getElementsByName('input_password')[0].value;
    var input_password_2 = document.getElementsByName('input_password')[1].value;

    // Добавить проверку на кол символов
    if (input_password_1 == input_password_2){
        ipcRenderer.send('newUser', {
            authorization: input_password_1
        });
    } else {
        alert('password err');
    }
});