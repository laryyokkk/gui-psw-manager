// https://www.npmjs.com/package/hybrid-crypto-js

const RSA = require('hybrid-crypto-js').RSA;
const Crypt = require('hybrid-crypto-js').Crypt;
const crypto = require('crypto');

const fileManager = require('./fileManager');

const entropy = 'nR918CIa72Jx*$ac}R}tGGe61NDbDD2iDP{yBiJvWN~wkft$qXun*7hlcbTsN%~dF3i3cB8MYlY@|ZpB}HLCOdOdO}74r8}nxkeNiCc3%k$SyBtkCENR8Tc{nLq1bKNpYw#BjZ*pdT{@69B|x*jgZfzoRwy$a39o3D${bE1pnrqokBSxt*L|Z~Ru#aN3d9HXOhk$4lz8LzcCTuRzm8fLrIX?f4ThJw%g*c61{DIW{uH*Kpc6YzbTe}gCSZ%1NivsI?Z*1q$ZS*n%}Lp$7YQrE}Qe*6Q4T#YofX7Wfk~W';

function GenerateKeys() {
    let rsa = new RSA();
    rsa.generateKeyPair(function (keyPair) {
        fileManager.SaveDataToFile('/public.key', keyPair.publicKey);
        fileManager.SaveDataToFile('/private.key', keyPair.privateKey);
    });
}

function GenerateAuthorization(authorization) {
    fileManager.SaveDataToFile('/authorization.key', crypto.createHash('md5').update(authorization).digest('hex'));
}

function CheckAuthorization(authorization) {
    if (fileManager.GetDataFromFile('/authorization.key') == crypto.createHash('md5').update(authorization).digest('hex')) return true;
}

function Encryption(data) {
    let crypt = new Crypt({
        rsaStandard: 'RSA-OAEP',
        entropy: entropy
    });
    let rsa = new RSA({
        entropy: entropy
    });
    return crypt.encrypt(fileManager.GetDataFromFile('/public.key'), data);
}



function Decryption(encrData) {
    let crypt = new Crypt({
        rsaStandard: 'RSA-OAEP',
        entropy: entropy
    });
    let rsa = new RSA({
        entropy: entropy
    });
    return crypt.decrypt(fileManager.GetDataFromFile('/private.key'), encrData).message;
}

module.exports = {
    GenerateKeys: GenerateKeys,
    GenerateAuthorization: GenerateAuthorization,
    CheckAuthorization: CheckAuthorization,
    Encryption: Encryption,
    Decryption: Decryption,
}