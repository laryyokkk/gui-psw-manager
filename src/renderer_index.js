const manager = require('./manager');

const input_find = document.getElementById('input_find');
const container_file = document.getElementById('files');
const container_index = document.getElementsByClassName('container_index')[0];

let data = manager.initJSON();
let activeElement = [];
let files = initFiles();

if (files) {
    showFiles();
}

function initFiles() {
    let arr_file = new Array();
    if (data.collections.length) {
        for (let id = 0; id < data.collections.length; id++) {
            if (data.collections[id]) {
                let file = document.createElement('div');
                file.className = 'file';
                file.addEventListener('click', () => {
                    if (activeElement[0]) {
                        activeElement[0].className = 'file';
                    }
                    activeElement = [file, id];
                    file.className = 'file active';

                    refreshIndex();
                    generateRow('company', data.collections[id].company, id);
                    generateRow('login', data.collections[id].login, id);
                    generateRow('password', data.collections[id].password, id);
                    generateRow('address', data.collections[id].address, id);
                    generateRow('mnemonic', data.collections[id].mnemonic, id);
                    generateRow('link', data.collections[id].link, id);
                    generateRow('restore_key', data.collections[id].restore_key, id);
                    generateRow('notes', data.collections[id].notes, id);

                    container_index.style.display = 'flex';
                });
                let file_title = document.createElement('p');
                file_title.className = 'file_title';
                file_title.innerHTML = data.collections[id].company;
                file.appendChild(file_title);

                let file_subtitle = document.createElement('p');
                file_subtitle.className = 'file_subtitle';
                file_subtitle.innerHTML = data.collections[id].login;
                file.appendChild(file_subtitle);

                arr_file.push(file);
            }

        }
        return arr_file;
    } else {
        console.log('Empty collections object');
        return;
    }
}

function showFiles(toShow = -1) {
    refreshFiles();
    if (files && toShow != -1) {
        for (let i = 0; i < toShow.length; i++) {
            container_file.append(files[toShow[i]]);
        }
    } else if (toShow == -1) {
        for (let i = 0; i < files.length; i++) {
            container_file.append(files[i]);
        }
    }
}

function generateRow(title, value, id) {
    let row = document.createElement('div');
    row.className = 'row';

    let legend = document.createElement('p');
    legend.className = 'legend';
    legend.innerHTML = title + ':';
    row.appendChild(legend);

    let input = document.createElement('input');
    input.name = '_input';
    input.value = checkValue(value);
    input.addEventListener('input', () => {
        data = manager.modifyByID(data, title, input.value, id);
    });
    input.addEventListener('change', () => {
        manager.saveDataToFile(data);
    })
    row.appendChild(input);

    container_index.appendChild(row);
}

function checkValue(value) {
    if (value == 'Company' || value == 'Login') {
        return '';
    }
    return value;
}


input_find.addEventListener('input', () => {
    if (input_find.value != '') {
        let toMark = manager.findAcc(data, input_find.value);
        if (toMark != -1) {
            showFiles(toMark);
        }
    } else {
        showFiles();
    }
})

function temp1() {
    refreshFiles();
    refreshIndex();
    data = manager.addStandardAccToJSON(data, 'Company', 'Login', '', '', '', '', '', '');
    files = initFiles();
    showFiles();
    container_index.style.display = 'none';
}

function temp2() {
    data = manager.deleteAccByID(data, activeElement[1]);
    manager.saveDataToFile(data);
    // refreshFiles();
    // refreshIndex();
    files = initFiles();
    showFiles();
    container_index.style.display = 'none';
}

function refreshFiles() {
    while (container_file.firstChild) {
        container_file.removeChild(container_file.firstChild);
    }
}

function refreshIndex() {
    while (container_index.firstChild) {
        container_index.removeChild(container_index.firstChild);
    }
}